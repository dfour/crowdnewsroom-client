# swagger_client.AuthApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_token_auth_post**](AuthApi.md#api_token_auth_post) | **POST** /api-token-auth/ | Get API token

# **api_token_auth_post**
> api_token_auth_post(username=username, password=password)

Get API token

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthApi()
username = 'username_example' # str |  (optional)
password = 'password_example' # str |  (optional)

try:
    # Get API token
    api_instance.api_token_auth_post(username=username, password=password)
except ApiException as e:
    print("Exception when calling AuthApi->api_token_auth_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  | [optional] 
 **password** | [**str**](.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

