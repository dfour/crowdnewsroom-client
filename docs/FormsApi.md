# swagger_client.FormsApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**forms_forms_form_responses_get**](FormsApi.md#forms_forms_form_responses_get) | **GET** /forms/forms/{form}/responses | get all form responses

# **forms_forms_form_responses_get**
> forms_forms_form_responses_get(form)

get all form responses

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: token_auth
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.FormsApi(swagger_client.ApiClient(configuration))
form = 'form_example' # str | 

try:
    # get all form responses
    api_instance.forms_forms_form_responses_get(form)
except ApiException as e:
    print("Exception when calling FormsApi->forms_forms_form_responses_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[token_auth](../README.md#token_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

